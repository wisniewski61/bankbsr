﻿using BankClient.BankService;
using BankClient.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BankClient
{
    /// <summary>
    /// Interaction logic for AccountWindow.xaml
    /// </summary>
    public partial class AccountWindow : Window
    {
        BankServiceClient bankServiceClient;
        User user;
        Account[] accounts;
        ObservableCollection<string> accountsList;
        ObservableCollection<HistoryOperation> historyList;

        public AccountWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize AccountWindow with given parameters
        /// </summary>
        /// <param name="bankServiceClient">object to connect SOAP bank service</param>
        /// <param name="user">current logged user</param>
        /// <param name="accounts">list of current logged user accounts</param>
        public AccountWindow(BankServiceClient bankServiceClient, User user, Account[] accounts) : this()
        {
            this.bankServiceClient = bankServiceClient;
            this.user = user;
            this.accounts = accounts;

            InitAccountsList();
            UpdateHistoryDg();
        }

        private void UpdateHistoryDg()
        {
            if (AccountsCB.SelectedValue == null)
                return;

            string selectedAccount = AccountsCB.SelectedValue.ToString();
            History[] history = bankServiceClient.GetAccountHistory(user, selectedAccount);
            historyList = InitHistoryList(history);

            HistoryDg.ItemsSource = historyList;
        }

        private ObservableCollection<HistoryOperation> InitHistoryList(History[] history)
        {
            ObservableCollection<HistoryOperation> hist = new ObservableCollection<HistoryOperation>();
            foreach (var h in history)
            {
                string amount = FormatCurrency(h.Amount);
                string balance = FormatCurrency(h.Balance);
                hist.Add(new HistoryOperation(h.Title, h.Source, amount, balance));
            }//for

            return hist;
        }

        private void InitAccountsList()
        {
            accountsList = new ObservableCollection<string>();
            foreach (Account a in accounts)
                accountsList.Add(a.Id);

            AccountsCB.ItemsSource = accountsList;

            AccountsCB.SelectionChanged += (sender, args) =>
            {
                UpdateBalanceTb();
            };
        }

        private void UpdateBalanceTb()
        {
            BalanceTB.Text = GetAccountBalance(AccountsCB.SelectedValue.ToString());
            UpdateHistoryDg();
        }

        private string GetAccountBalance(string text)
        {
            string balance = String.Empty;
            foreach(var a in accounts)
            {
                if(a.Id.Equals(text))
                {
                    balance = FormatCurrency(a.Balance);
                }//if
            }//for

            return balance + " PLN";
        }

        private string FormatCurrency(long balance)
        {
            string balanceStr = (balance / (long)100).ToString() + ".";
            long rest = balance % (long)100;
            if (rest < 0)
                rest *= -1;
            if (rest <= 9)
                balanceStr += "0";
            balanceStr += rest.ToString();

            return balanceStr;
        }

        // source: https://stackoverflow.com/questions/1268552/how-do-i-get-a-textbox-to-only-accept-numeric-input-in-wpf/1268648#1268648
        private void PaymentTB_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        // source: https://stackoverflow.com/questions/1268552/how-do-i-get-a-textbox-to-only-accept-numeric-input-in-wpf/1268648#1268648
        private bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.,-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void PaymentTB_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void PaymentBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!isAccountSelected())
                return;
            if (PaymentTB.Text.Length == 0)
            {
                MessageBox.Show("Payment amount cannot be empty!");
                return;
            }//if

            string[] amountStr = PaymentTB.Text.Split(',');
            if(amountStr.Length < 2)
                amountStr = PaymentTB.Text.Split('.');

            if (amountStr.Length == 2)
            {
                if(amountStr[0].Length > 0 && amountStr[1].Length > 0)
                {
                    if(amountStr[1].Length > 2)
                    {
                        MessageBox.Show("Incorrect payment amount!");
                        return;
                    }//if
                    string temp = amountStr[0] + amountStr[1];
                    MakePayment(temp);
                    return;
                }//if
            }//if

            if (amountStr.Length == 1)
            {
                MakePayment(amountStr[0]+"00");
                return;
            }//if

            MessageBox.Show("Payment amount have to be a number!");
        }

        private bool isAccountSelected()
        {
            if (AccountsCB.SelectedValue == null)
            {
                MessageBox.Show("You have to select account");
                return false;
            }//if

            return true;
        }

        private void MakePayment(string valueStr)
        {
            long amount = 0;
            if (long.TryParse(valueStr, out amount))
            {
                if (amount != 0)
                {
                    SendPayment(amount);
                    return;
                }//if

                MessageBox.Show("Amount cannot be equal to zero");
                return;
            }//if

            MessageBox.Show("Payment amount have to be a number!");
        }

        private void SendPayment(long amount)
        {
            try
            {
                string currentAccount = AccountsCB.SelectedValue.ToString();
                long balance = bankServiceClient.Payment(user, currentAccount, amount);
                UpdateAccountBalance(currentAccount, balance);
            }//try
            catch(FaultException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void UpdateAccountBalance(string accountNumber, long balance)
        {
            foreach(var a in accounts)
            {
                if(a.Id.Equals(accountNumber))
                {
                    a.Balance = balance;
                    break;
                }//if
            }//for

            UpdateBalanceTb();
        }

        private void TransferBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!isAccountSelected())
                return;

            string target = TargetAccountTb.Text;
            if(target.Length==0)
            {
                MessageBox.Show("Type target account number");
                return;
            }//if

            string title = TitleTb.Text;
            if (title.Length == 0)
            {
                MessageBox.Show("Title cannot be empty");
                return;
            }//if

            string[] amountStr = TransferAmountTb.Text.Split(',');
            if (amountStr.Length < 2)
                amountStr = TransferAmountTb.Text.Split('.');

            if (amountStr.Length == 2)
            {
                if (amountStr[0].Length > 0 && amountStr[1].Length > 0)
                {
                    if (amountStr[1].Length > 2)
                    {
                        MessageBox.Show("Incorrect transfer amount!");
                        return;
                    }//if
                    string temp = amountStr[0] + amountStr[1];
                    MakeTransfer(temp, target, title);
                    return;
                }//if
            }//if

            if (amountStr.Length == 1)
            {
                MakeTransfer(amountStr[0] + "00", target, title);
                return;
            }//if

            MessageBox.Show("Payment amount have to be a number!");
        }

        private void MakeTransfer(string v, string target, string title)
        {
            long amount = 0;
            if (long.TryParse(v, out amount))
            {
                if (amount != 0)
                {
                    SendTransfer(amount, target, title);
                    return;
                }//if

                MessageBox.Show("Amount cannot be equal to zero");
                return;
            }//if

            MessageBox.Show("Transfer amount have to be a number!");
        }

        private void SendTransfer(long amount, string target, string title)
        {
            try
            {
                string currentAccount = AccountsCB.SelectedValue.ToString();
                Transfer transfer = PrepareTransfer(currentAccount, target, amount, title);
                long balance = bankServiceClient.Transfer(user, transfer);
                UpdateAccountBalance(currentAccount, balance);
            }//try
            catch (FaultException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private Transfer PrepareTransfer(string currentAccount, string target, long amount, string title)
        {
            Transfer t = new Transfer();
            t.From = currentAccount;
            t.To = target;
            t.Title = title;
            t.Amount = amount;

            return t;
        }
    }
}
