﻿using BankClient.BankService;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows;

namespace BankClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BankServiceClient bank = new BankServiceClient();
                User user = new User();
                user.Name = UsernameTB.Text;
                user.Password = PasswordTB.Password;

                if (bank.Auth(user))
                {
                    Account[] accounts = bank.GetUserAccounts(user);
                    new AccountWindow(bank, user, accounts).Show();
                    this.Close();
                }//if
                else
                    MessageBox.Show("Wrong username or password", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }//try
            catch(Exception ex)
            {
                if(ex is EndpointNotFoundException || ex is FaultException)
                    MessageBox.Show("Service unavaliable", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                    MessageBox.Show("Internal error", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }//catch
        }
    }
}
