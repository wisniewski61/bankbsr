﻿using BankClient.BankService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankClient.Models
{
    /// <summary>
    /// Data model for history operation grid
    /// </summary>
    class HistoryOperation
    {
        public string Title { get; set; }
        public string Amount { get; set; }
        public string Source { get; set; }
        public string Balance { get; set; }

        public HistoryOperation(string title, string source, string amount, string balance)
        {
            Title = title;
            Amount = amount;
            Source = source;
            Balance = balance;
        }
    }
}
