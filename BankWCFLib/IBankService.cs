﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankWCFLib
{
    /// <summary>
    /// SOAP bank service interface specification
    /// </summary>
    [ServiceContract]
    public interface IBankService
    {
        /// <summary>
        /// Check if user credentials are correct
        /// </summary>
        /// <param name="user">user to check</param>
        /// <returns>true if user name nad password are valid, else false </returns>
        [OperationContract]
        bool Auth(User user);

        /// <summary>
        /// Get list of user accounts
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>list of user accounts</returns>
        [OperationContract]
        List<Account> GetUserAccounts(User user);

        /// <summary>
        /// User own payment
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="accountId">account for payment</param>
        /// <param name="amount">amount to pay</param>
        /// <returns>account balance after operation</returns>
        [OperationContract]
        long Payment(User user, string accountId, long amount);

        /// <summary>
        /// Get operation history for bank account
        /// </summary>
        /// <param name="user">User - account owner</param>
        /// <param name="accountId">account id</param>
        /// <returns>list of history object for given account id</returns>
        [OperationContract]
        List<History> GetAccountHistory(User user, string accountId);

        /// <summary>
        /// Transfer money to another bank account
        /// </summary>
        /// <param name="user">user - source account owner</param>
        /// <param name="transfer">transfer details</param>
        /// <returns>source account balance</returns>
        [OperationContract]
        long Transfer(User user, Transfer transfer);
    }

    /// <summary>
    /// User data contract class
    /// </summary>
    [DataContract]
    public class User
    {
        /// <summary>
        /// Initializes user with given name and password
        /// </summary>
        /// <param name="name"></param>
        /// <param name="password"></param>
        public User(string name, string password)
        {
            this.name = name;
            this.password = password;
        }

        public User()
        {
            name = "";
            password = "";
        }

        /// <summary>
        /// User id
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        string name;
        string password;

        /// <summary>
        /// Username
        /// </summary>
        [DataMember]
        [BsonElement("Name")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// User password
        /// </summary>
        [DataMember]
        [BsonElement("Password")]
        public string Password { get => this.password; set => this.password = value; }
    }

    /// <summary>
    /// Account data contract class
    /// </summary>
    [DataContract]
    public class Account
    {
        /// <summary>
        /// Account id
        /// </summary>
        [DataMember]
        [BsonId]
        public string Id { get; set; }

        /// <summary>
        /// User id
        /// </summary>
        [BsonElement]
        public string UserId { get; set; }

        /// <summary>
        /// account current balance
        /// </summary>
        [DataMember]
        [BsonElement]
        public long Balance { get; set; }

        public Account()
        {
            Id = "";
            UserId = "";
            Balance = 0;
        }

        /// <summary>
        /// initializes new account with id and user id
        /// </summary>
        /// <param name="id">account id</param>
        /// <param name="userId">user id - account owner</param>
        public Account(string id, string userId)
        {
            this.Id = id;
            this.UserId = userId;
            this.Balance = 0;
        }
    }

    /// <summary>
    /// History data contract class
    /// </summary>
    [DataContract]
    public class History
    {
        /// <summary>
        /// history object id
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// account id connected with this operation
        /// </summary>
        [BsonElement]
        [DataMember]
        public string AccountId { get; set; }

        /// <summary>
        /// Operation money amount
        /// </summary>
        [BsonElement]
        [DataMember]
        public long Amount { get; set; }

        /// <summary>
        /// Operation title
        /// </summary>
        [BsonElement]
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Operation source account id or text "payment" if source is own payment
        /// </summary>
        [BsonElement]
        [DataMember]
        public string Source { get; set; }

        /// <summary>
        /// Account balance after operation
        /// </summary>
        [BsonElement]
        [DataMember]
        public long Balance { get; set; }

        public History()
        {
            AccountId = string.Empty;
            Title = string.Empty;
            Source = string.Empty;
            Amount = 0;
            Balance = 0;
        }

        /// <summary>
        /// Init new history object with given parameters
        /// </summary>
        /// <param name="accountId">id of account connected with operation</param>
        /// <param name="title">operation title</param>
        /// <param name="source">operation source: account id or text "payment"</param>
        /// <param name="amount">money amount</param>
        /// <param name="balance">account balance after operation for account with given accountId</param>
        public History(string accountId, string title, string source, long amount, long balance)
        {
            AccountId = accountId;
            Title = title;
            Source = source;
            Amount = amount;
            Balance = balance;
        }
    }

    /// <summary>
    /// Transfer data contract class
    /// </summary>
    [DataContract]
    public class Transfer
    {
        /// <summary>
        /// Source account
        /// </summary>
        [DataMember]
        public string From { get; set; }

        /// <summary>
        /// Target account
        /// </summary>
        [DataMember]
        public string To { get; set; }

        /// <summary>
        /// Money amount
        /// </summary>
        [DataMember]
        public long Amount { get; set; }

        /// <summary>
        /// Transfer title
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        public Transfer()
        {
            Amount = 0;
        }

        /// <summary>
        /// Init transfer with given parameters
        /// </summary>
        /// <param name="fromAccount">source account</param>
        /// <param name="toAccount">target account</param>
        /// <param name="amount">money amount</param>
        /// <param name="title">transfer title</param>
        public Transfer(string fromAccount, string toAccount, long amount, string title)
        {
            this.From = fromAccount;
            this.To = toAccount;
            this.Amount = amount;
            this.Title = title;
        }
    }
}
