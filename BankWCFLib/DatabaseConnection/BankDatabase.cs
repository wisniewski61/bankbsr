﻿using MongoDB.Driver;
using System;

namespace BankWCFLib.DatabaseConnection
{
    public class BankDatabase
    {
        protected static readonly string dbname = "bankbsr";
        protected MongoClient mongoClient;
        protected IMongoDatabase mongodb;

        public BankDatabase()
        {
            mongoClient = new MongoClient("mongodb://localhost:8004");
            mongodb = mongoClient.GetDatabase(dbname);
        }

        /// <summary>
        /// Drop current database
        /// </summary>
        public void DropDatabase()
        {
            mongoClient.DropDatabase(dbname);
            mongodb = null;
        }

        /// <summary>
        /// Create new database
        /// </summary>
        public void CreateDatabase()
        {
            mongodb = mongoClient.GetDatabase(dbname);
        }
    }
}
