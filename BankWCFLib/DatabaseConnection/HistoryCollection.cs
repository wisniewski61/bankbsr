﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankWCFLib.DatabaseConnection
{
    public class HistoryCollection : BankDatabase
    {
        static readonly string collectionName = "history";
        public HistoryCollection() : base()
        {
        }

        /// <summary>
        /// Create empty collection
        /// </summary>
        public void Seed()
        {
            mongodb.CreateCollection(collectionName);
        }

        /// <summary>
        /// Get operation history for specified account
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <returns>list of history objects for specified account id</returns>
        public List<History> GetAccountHistory(string accountId)
        {
            var filter = Builders<History>.Filter.Eq("AccountId", accountId);
            return mongodb.GetCollection<History>(collectionName).FindSync(filter).ToList();
        }

        /// <summary>
        /// Add new history object to database collection
        /// </summary>
        /// <param name="history">object to add</param>
        public void Add(History history)
        {
            mongodb.GetCollection<History>(collectionName).InsertOne(history);
        }
    }
}
