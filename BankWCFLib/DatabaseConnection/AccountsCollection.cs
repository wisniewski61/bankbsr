﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BankWCFLib.DatabaseConnection
{
    public class AccountsCollection : BankDatabase
    {
        static readonly string collectionName = "Accounts";
        static readonly string myBank = "00116912";
        static readonly string accountPrefixPL = "2521";

        public AccountsCollection() : base()
        {
        }

        /// <summary>
        /// Create collection and initialize it with some basic data.
        /// </summary>
        /// <param name="users">existing users in database</param>
        public void Seed(List<User> users)
        {
            mongodb.CreateCollection(collectionName);
            var accounts = mongodb.GetCollection<Account>(collectionName);
            int lastAccountNum = 1;

            foreach(User u in users)
            {
                List<Account> accountsList = new List<Account>();
                
                accountsList.Add(new Account(GenerateAccountNumber(lastAccountNum++), u.Id));
                accountsList.Add(new Account(GenerateAccountNumber(lastAccountNum++), u.Id));
                accounts.InsertMany(accountsList);
            }
        }

        private string GenerateAccountNumber(int lastAccNum)
        {
            string accountNumber = String.Empty;
            string num = lastAccNum.ToString();
            StringBuilder numfiller = new StringBuilder();
            int toFill = 16 - num.Length;
            while(numfiller.Length < toFill)
            {
                numfiller.Append("0");
            }//while

            num = myBank + numfiller + lastAccNum + accountPrefixPL + "00";
            BigInteger controlSum = new BigInteger(98) - (BigInteger.Parse(num) % new BigInteger(97));
            accountNumber = controlSum + myBank + numfiller + lastAccNum;

            VerifyAccountNumber(accountNumber);

            return accountNumber;
        }

        /// <summary>
        /// Update specified account balance
        /// </summary>
        /// <param name="account">Account for update</param>
        public void UpdateAccount(Account account)
        {
            var filterBuilder = Builders<Account>.Filter;
            var filter = filterBuilder.And(filterBuilder.Eq("Id", account.Id), filterBuilder.Eq("UserId", account.UserId));
            var update = Builders<Account>.Update.Set("Balance", account.Balance);

            mongodb.GetCollection<Account>(collectionName).UpdateOne(filter, update);
        }

        /// <summary>
        /// Verify if given text is correct bank account number (NRB)
        /// </summary>
        /// <param name="accountNumber">text to check</param>
        /// <returns>verified account number if accountNumber is correct</returns>
        public string VerifyAccountNumber(string accountNumber)
        {
            string toVerify = accountNumber.Replace(" ", String.Empty);
            if (toVerify.Length != 26)
                throw new FaultException("Bank account number must be 26 characters long");

            BigInteger number = BigInteger.Parse(accountNumber.Substring(2) + accountPrefixPL + accountNumber.Substring(0, 2));
            BigInteger result = number % new BigInteger(97);
            if (!result.Equals(1))
                throw new FaultException(accountNumber+" is not valid bank account number");

            return toVerify;
        }

        /// <summary>
        /// Gets list of user bank accounts
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>list of user bank accounts</returns>
        public List<Account> GetUserAccountsById(string userId)
        {
            var filter = Builders<Account>.Filter.Eq("UserId", userId);
            return mongodb.GetCollection<Account>(collectionName).FindSync(filter).ToList();
        }

        /// <summary>
        /// Get user account by account id and user id
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="userId">user id</param>
        /// <returns>User account that matches provided parameters</returns>
        public Account GetAccountById(string accountId, string userId)
        {
            var filterBuilder = Builders<Account>.Filter;
            var filter = filterBuilder.And(filterBuilder.Eq("Id", accountId), filterBuilder.Eq("UserId", userId));
            return mongodb.GetCollection<Account>(collectionName).FindSync(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get user account by id
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <returns>User bank account that matches account id</returns>
        public Account GetAccountById(string accountId)
        {
            var filter = Builders<Account>.Filter.Eq("Id", accountId);
            return mongodb.GetCollection<Account>(collectionName).FindSync(filter).FirstOrDefault();
        }
    }
}
