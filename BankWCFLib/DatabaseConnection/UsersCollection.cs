﻿using BankWCFLib;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace BankWCFLib.DatabaseConnection
{
    public class UsersCollection : BankDatabase
    {
        static readonly string collectionName = "Users";

        public UsersCollection() : base()
        {
        }

        /// <summary>
        /// Create collection and initialize it with some basic data
        /// </summary>
        public void Seed()
        {
            mongodb.CreateCollection(collectionName);
            Add(new User("user", "pass"));
            Add(new User("user1", "password"));
        }

        /// <summary>
        /// Get all users list from database
        /// </summary>
        /// <returns>all users list</returns>
        public List<User> GetAll()
        {
            return mongodb.GetCollection<User>(collectionName).AsQueryable().ToList();
        }

        /// <summary>
        /// Add new user to database collection
        /// </summary>
        /// <param name="user">User to add</param>
        public void Add(User user)
        {
            if (Get(user.Name) != null)
                throw new ArgumentException("User alredy exists");

            var users = mongodb.GetCollection<User>(collectionName);
            users.InsertOne(user);
        }

        /// <summary>
        /// Get one user by name
        /// </summary>
        /// <param name="name">name to filter</param>
        /// <returns>user with given name</returns>
        public User Get(string name)
        {
            var filter = Builders<User>.Filter.Eq("Name", name);
            return mongodb.GetCollection<User>(collectionName).FindSync(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get one user by name and password
        /// </summary>
        /// <param name="user">User object with filled Name and Password properties</param>
        /// <returns>User with given name and password</returns>
        public User Get(User user)
        {
            var filterBuilder = Builders<User>.Filter;
            var filter = filterBuilder.And(filterBuilder.Eq("Name", user.Name), filterBuilder.Eq("Password", user.Password));
            return mongodb.GetCollection<User>(collectionName).FindSync(filter).FirstOrDefault();
        }
    }
}
