﻿using BankWCFLib.DatabaseConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankWCFLib
{
    public class BankService : IBankService
    {
        UsersCollection usersCollection = new UsersCollection();
        AccountsCollection accountsCollection = new AccountsCollection();
        HistoryCollection historyCollection = new HistoryCollection();

        /// <summary>
        /// Check if user credentials are correct
        /// </summary>
        /// <param name="user">user to check</param>
        /// <returns>true if user name nad password are valid, else false </returns>
        public bool Auth(User user)
        {
            if (usersCollection.Get(user)!=null)
                return true;
            return false;
        }

        /// <summary>
        /// Check if user is authenticated. Throws FaultException if user is not authenticated
        /// </summary>
        /// <param name="user">user to check</param>
        protected void VerifyUserAuthentication(User user)
        {
            if (Auth(user))
                return;

            throw new FaultException("User not authenticated");
        }

        /// <summary>
        /// Get list of user accounts
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>list of user accounts</returns>
        public List<Account> GetUserAccounts(User user)
        {
            VerifyUserAuthentication(user);

            string userId = usersCollection.Get(user.Name).Id;
            return accountsCollection.GetUserAccountsById(userId);
        }

        /// <summary>
        /// User own payment
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="accountId">account for payment</param>
        /// <param name="amount">amount to pay</param>
        /// <returns>account balance after operation</returns>
        public long Payment(User user, string accountId, long amount)
        {
            VerifyUserAuthentication(user);

            user = usersCollection.Get(user.Name);
            Account account = accountsCollection.GetAccountById(accountId, user.Id);
            if (account == null)
                throw new FaultException("This account doesnt exists or you are not its owner");

            long currentBalance = account.Balance;
            currentBalance += amount;

            if (currentBalance < 0)
                throw new FaultException("You have not enought money to complete this operation");

            account.Balance = currentBalance;
            accountsCollection.UpdateAccount(account);
            historyCollection.Add(new History(account.Id, "payment", "payment", amount, currentBalance));

            return currentBalance;
        }

        /// <summary>
        /// Get operation history for bank account
        /// </summary>
        /// <param name="user">User - account owner</param>
        /// <param name="accountId">account id</param>
        /// <returns>list of history object for given account id</returns>
        public List<History> GetAccountHistory(User user, string accountId)
        {
            VerifyUserAuthentication(user);
            user = usersCollection.Get(user.Name);

            if(accountsCollection.GetAccountById(accountId, user.Id) == null)
            {
                throw new FaultException("This account doesnt exists or you are not its owner");
            }//if

            return historyCollection.GetAccountHistory(accountId);
        }

        /// <summary>
        /// Transfer money to another bank account
        /// </summary>
        /// <param name="user">user - source account owner</param>
        /// <param name="transfer">transfer details</param>
        /// <returns>source account balance</returns>
        public long Transfer(User user, Transfer transfer)
        {
            VerifyUserAuthentication(user);
            user = usersCollection.Get(user.Name);

            transfer.From = accountsCollection.VerifyAccountNumber(transfer.From);
            transfer.To = accountsCollection.VerifyAccountNumber(transfer.To);

            Account account = accountsCollection.GetAccountById(transfer.From, user.Id);
            if (account == null)
                throw new FaultException("This account doesnt exists or you are not its owner");

            if (transfer.From.Equals(transfer.To))
                throw new FaultException("Source and destination accounts numbers are the same");

            if (transfer.Amount <= 0)
                throw new FaultException("Transfer amount must be greater than zero");

            long currentBalance = account.Balance;
            currentBalance -= transfer.Amount;
            if(currentBalance < 0)
                throw new FaultException("You have not enought money to complete this operation");

            Account targetAccount = accountsCollection.GetAccountById(transfer.To);
            if (targetAccount != null)
                return InternalTransfer(user, account, targetAccount, transfer.Amount, transfer.Title);
            else
                return ExternalTransfer(user, account, transfer.To, transfer.Amount, transfer.Title);

           throw new FaultException("Unknown target bank account");
        }

        /// <summary>
        /// Transfer money to another bank
        /// </summary>
        /// <param name="user">User - source account owner</param>
        /// <param name="account">source account</param>
        /// <param name="to">target account id</param>
        /// <param name="amount">amount of money to transfer</param>
        /// <param name="title">transfer title</param>
        /// <returns>source account balance</returns>
        private long ExternalTransfer(User user, Account account, string to, long amount, string title)
        {
            throw new FaultException("External transfer not avaliable for free accounts. We are sorry...");
        }

        private long InternalTransfer(User user, Account account, Account targetAccount, long amount, string title)
        {
            account.Balance -= amount;
            accountsCollection.UpdateAccount(account);
            historyCollection.Add(new History(account.Id, title, targetAccount.Id, -amount, account.Balance));

            targetAccount.Balance += amount;
            accountsCollection.UpdateAccount(targetAccount);
            historyCollection.Add(new History(targetAccount.Id, title, account.Id, amount, targetAccount.Balance));

            return account.Balance;
        }
    }
}
