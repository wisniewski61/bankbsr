﻿using BankWCFLib;
using BankWCFLib.DatabaseConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitBankDB
{
    class Program
    {
        /// <summary>
        /// Initialize database
        /// </summary>
        /// <param name="args">args from command prompt</param>
        static void Main(string[] args)
        {
            BankDatabase bd = new BankDatabase();
            bd.DropDatabase();
            bd.CreateDatabase();

            UsersCollection users = new UsersCollection();
            users.Seed();

            List<User> usersList = users.GetAll();
            AccountsCollection accountsCollection = new AccountsCollection();
            accountsCollection.Seed(usersList);

            new HistoryCollection().Seed();
        }
    }
}
