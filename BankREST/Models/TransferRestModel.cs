﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BankREST.Models
{
    /// <summary>
    /// Transfer model object
    /// </summary>
    public class TransferRestModel
    {
        /// <summary>
        /// source account
        /// </summary>
        public string source_account { get; set; }

        /// <summary>
        /// transfer money amount
        /// </summary>
        public int amount { get; set; }

        /// <summary>
        /// Transfer title
        /// </summary>
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Title have to be at least 1 and maximum 255 characters long")]
        public string title { get; set; }

        /// <summary>
        /// Source user name
        /// </summary>
        [StringLength(255, MinimumLength = 1, ErrorMessage = "source_name have to be at least 1 and maximum 255 characters long")]
        public string source_name { get; set; }

        /// <summary>
        /// Destination user name
        /// </summary>
        [StringLength(255, MinimumLength = 1, ErrorMessage = "destination_name have to be at least 1 and maximum 255 characters long")]
        public string destination_name { get; set; }
    }
}