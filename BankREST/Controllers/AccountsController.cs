﻿using BankREST.Models;
using BankWCFLib;
using BankWCFLib.DatabaseConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BankREST.Controllers
{
    /// <summary>
    /// interbanks operations controller
    /// </summary>
    public class AccountsController : ApiController
    {
        /// <summary>
        /// Transfer money to bank account in this bank
        /// </summary>
        /// <param name="id">target account id - this account will receive money</param>
        /// <param name="transfer">transfer details (source account, title, amount of money, etc.)</param>
        /// <returns></returns>
        [Route("accounts/{id}/history")]
        public HttpResponseMessage Post(string id, TransferRestModel transfer)
        {
            AccountsCollection ac = new AccountsCollection();
            HistoryCollection hc = new HistoryCollection();

            Account account = ac.GetAccountById(id);
            if (account == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }//if

            if (transfer.amount <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    error = "Amount must be greater than 0",
                    filed = "amount"
                });
            }

            account.Balance += transfer.amount;
            ac.UpdateAccount(account);
            hc.Add(new History(account.Id, transfer.title, transfer.source_account, transfer.amount, account.Balance));

            return Request.CreateResponse(HttpStatusCode.Created);
        }
    }
}
